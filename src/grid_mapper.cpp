#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"
#include <cstdlib> // Needed for rand()
#include <ctime> // Needed to seed random number generator with a time value
#include <utility> //needed for swap()
#include <boost/thread/mutex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <tf/transform_listener.h>


#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

using namespace boost::posix_time;


const static int GRID_SCALAR = 20;

struct Pose {
    double x; // in simulated Stage units
    double y; // in simulated Stage units
    double heading; // in radians
    ros::Time t; // last received time

    // Construct a default pose object with the time set to 1970-01-01
    Pose() : x(0), y(0), heading(0), t(0.0) {};

    // Process incoming pose message for current robot
    void poseCallback(const nav_msgs::Odometry::ConstPtr &msg) {
        double roll, pitch;

        x = msg->pose.pose.position.x * GRID_SCALAR;
        y = msg->pose.pose.position.y * GRID_SCALAR;
        tf::Quaternion q = tf::Quaternion(
                msg->pose.pose.orientation.x,
                msg->pose.pose.orientation.y,
                msg->pose.pose.orientation.z,
                msg->pose.pose.orientation.w);
        tf::Matrix3x3(q).getRPY(roll, pitch, heading);
        t = msg->header.stamp;

    };
};

class GridMapper {
public:
    // Construct a new occupancy grid mapper  object and hook up
    // this ROS node to the simulated robot's pose, velocity control,
    // and laser topics
    GridMapper(ros::NodeHandle &nh, int width, int height) :
            canvas(height, width, CV_8UC1), nh(nh) {
        // Initialize random time generator
        srand(time(NULL));

        // Create resizeable named window
        cv::namedWindow("Occupancy Grid Canvas", CV_WINDOW_NORMAL | CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);

    };


    // Save a snapshot of the occupancy grid canvas
    // NOTE: image is saved to same folder where code was executed
    void saveSnapshot() {
        std::string filename = "grid_" + to_iso_string(second_clock::local_time()) + ".png";
        canvasMutex.lock();
        cv::imwrite(filename, canvas);
        canvasMutex.unlock();
    };


    // Update grayscale intensity on canvas pixel (x, y) (in robot coordinate frame)
    void plot(int x, int y, char value) {
        canvasMutex.lock();
        x = x + canvas.rows / 2;
        y = -y + canvas.cols / 2;
        if (x >= 0 && x < canvas.rows && y >= 0 && y < canvas.cols) {
            canvas.at<char>(y, x) = value;
        }
        canvasMutex.unlock();
        //ROS_INFO_STREAM("Canvas: " << canvas.rows << ", " << canvas.cols);
    };

    // Update grayscale intensity on canvas pixel (x, y) (in image coordinate frame)
    void plotImg(int x, int y, char value) {
        canvasMutex.lock();
        if (x >= 0 && x < canvas.cols && y >= 0 && y < canvas.rows) {
            canvas.at<char>(y, x) = value;
        }
        canvasMutex.unlock();
        ROS_INFO_STREAM("PlotImg: " << x << ", " << y);
    };

    bool isCellEqualToStatus(int x, int y, char value) {
        x = x + canvas.rows / 2;
        y = -y + canvas.cols / 2;
        return canvas.at<char>(y, x) == value;
    }

    void plotLine(int x1, int y1, int x2, int y2, char color) {
        //Bresenham's line algorithm
        const bool isSteep = std::abs(y2 - y1) > std::abs(x2 - x1);
        if (isSteep) {
            std::swap(x1, y1);
            std::swap(x2, y2);
        }

        if (x1 > x2) {
            std::swap(x1, x2);
            std::swap(y1, y2);
        }

        int dX = x2 - x1;
        int dY = std::abs(y2 - y1);
        int err = dX / 2;
        int yStep = (y1 < y2) ? 1 : -1;
        int y = y1;

        for (int x = x1; x <= x2; x++) {

            if (isSteep) {// && canvas.at<char>(y, x) == CELL_UNKNOWN) {
                plot(y, x, color);
            } else {// if (canvas.at<char>(x, y) == CELL_UNKNOWN) {
                plot(x, y, color);
            }

            err = err - dY;
            if (err < 0) {
                y += yStep;
                err += dX;
            }
        }

    };


    void laserPoseCallback(const nav_msgs::Odometry::ConstPtr &odo_msg,
                           const sensor_msgs::LaserScan::ConstPtr &laser_msg) {

        currPose.poseCallback(odo_msg);
        laserCallback(laser_msg);
    }


    // Process incoming laser scan message
    void laserCallback(const sensor_msgs::LaserScan::ConstPtr &msg) {

        //ROS_INFO_STREAM("Begin Laser Scan");
        ROS_INFO_STREAM("Time delay: " << msg->header.stamp - currPose.t);

        int prevX, prevY;
        for (int i = 0; i < msg->ranges.size(); i++) {
            float range = msg->ranges[i] * GRID_SCALAR;

            double angle = currPose.heading +
                    msg->angle_min +
                    (msg->angle_increment * (i + 1));

            if (range > msg->range_min * GRID_SCALAR) {


                int x = range * cos(angle) + currPose.x;
                int y = range * sin(angle) + currPose.y;

                if (x != prevX && y != prevY) {
                    plotLine(currPose.x, currPose.y, x, y, CELL_FREE);
                    if (range < msg->range_max * GRID_SCALAR) {
                        plot(x, y, CELL_OCCUPIED);
                    }
                }

            }
        }//end ranges loop
    };

    // Main FSM loop for ensuring that ROS messages are
    // processed in a timely manner, and also for sending
    // velocity controls to the simulated robot based on the FSM state
    void spin() {

        //message_filters::Subscriber<nav_msgs::Odometry> pose_sub(nh, "base_pose_ground_truth", 1);
        //message_filters::Subscriber<sensor_msgs::LaserScan> laser_sub(nh, "base_scan", 1);
        message_filters::Subscriber<nav_msgs::Odometry> pose_sub(nh, "base_pose_ground_truth", 1);
        message_filters::Subscriber<sensor_msgs::LaserScan> laser_sub(nh, "base_scan", 1);


        typedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry, sensor_msgs::LaserScan> approxSyncPolicy;
        // ApproximateTime takes a queue size as its constructor argument, hence MySyncPolicy(10)
        //message_filters::Synchronizer<approxSyncPolicy> approxSync(approxSyncPolicy(10), pose_sub, laser_sub);
        //sync = &approxSync;
        message_filters::Synchronizer<approxSyncPolicy> sync(approxSyncPolicy(10), pose_sub, laser_sub);
        //message_filters::TimeSynchronizer<nav_msgs::Odometry, sensor_msgs::LaserScan> sync(approxSyncPolicy(10), pose_sub, laser_sub);

        //sync.setAgePenalty(1.0);
        sync.registerCallback(boost::bind(&GridMapper::laserPoseCallback, this, _1, _2));


        int key = 0;
        ros::Rate rate(SPIN_RATE_HZ);
        // Initialize all pixel values in canvas to CELL_UNKNOWN
        canvasMutex.lock();
        canvas = cv::Scalar(CELL_UNKNOWN);
        canvasMutex.unlock();

        while (ros::ok()) { // Keep spinning loop until user presses Ctrl+C

            plot(currPose.x, currPose.y, CELL_ROBOT);

            if (currPose.t.sec != 0 && ((int) currPose.x != (int) prevPose.x ||
                                        (int) currPose.y != (int) prevPose.y)) {
                plot(currPose.x, currPose.y, CELL_ROBOT);
                plot(prevPose.x, prevPose.y, CELL_FREE);

                prevPose = currPose;
            }

            cv::imshow("Occupancy Grid Canvas", canvas);
            ros::spinOnce(); // Need to call this function often to allow ROS to process incoming messages
            key = cv::waitKey(1000 / SPIN_RATE_HZ); // Obtain keypress from user; wait at most N milliseconds
            if (key == 'x' || key == 'X') {
                break;
            } else if (key == ' ') {
                saveSnapshot();
            }


            ros::spinOnce();
            rate.sleep();
        }

        ros::shutdown(); // Ensure that this ROS node shuts down properly
    };

    const static int SPIN_RATE_HZ = 10;

    const static char CELL_OCCUPIED = 0;
    const static char CELL_UNKNOWN = 86;
    const static char CELL_FREE = 172;
    const static char CELL_ROBOT = 255;


    //const static int GRID_WIDTH = 10000;


protected:

    Pose currPose;
    Pose prevPose;

    cv::Mat canvas; // Occupancy grid canvas
    boost::mutex canvasMutex; // Mutex for occupancy grid canvas object

    message_filters::Subscriber<nav_msgs::Odometry> pose_sub;
    message_filters::Subscriber<sensor_msgs::LaserScan> laser_sub;

    //rtypedef message_filters::sync_policies::ApproximateTime<nav_msgs::Odometry, sensor_msgs::LaserScan> approxSyncPolicy;
    //message_filters::Synchronizer<approxSyncPolicy> sync;

    ros::NodeHandle &nh;
};


int main(int argc, char **argv) {
    int width, height;
    bool printUsage = false;

    // Parse and validate input arguments
    if (argc <= 2) {
        printUsage = true;
    } else {
        try {
            width = boost::lexical_cast<int>(argv[1]);
            height = boost::lexical_cast<int>(argv[2]);

            if (width <= 0) { printUsage = true; }
            else if (height <= 0) { printUsage = true; }
        } catch (std::exception err) {
            printUsage = true;
        }
    }
    if (printUsage) {
        std::cout << "Usage: " << argv[0] << " [CANVAS_WIDTH] [CANVAS_HEIGHT]" << std::endl;
        return EXIT_FAILURE;
    }

    ros::init(argc, argv, "grid_mapper"); // Initiate ROS node
    ros::NodeHandle n; // Create default handle
    GridMapper robbie(n, width, height); // Create new grid mapper object
    robbie.spin(); // Execute FSM loop

    return EXIT_SUCCESS;
};
