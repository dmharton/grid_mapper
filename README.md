# Grid Mapper #

Grid_mapper is a ROS node that populates a grid map using odometry and laser scan data. The occupied space was determined by ray-tracing from the robot to each of the scan points using Bresenham’s line algorithm. 

The dark gray represents unknown space, the light gray represents free space, black is the occupied space, and white is the robot's current position. In the swearingen map, white was also used to mark found intersections.


In the simulator, it was difficult to perfectly sync up the odometry and laser data which resulted in skewed map data, especially while rotating. Some of this could be alleviated by reducing the robot’s speed, limiting the laser’s range, and using the ApproximateTime message filter. While these techniques helped, neither of them were perfect solutions. 

The program can be run by the following commands in separate terminals:
*   roscore
*   rosrun stage_ros stageros $(rospack find stage_ros)/world/cave_single.world
*   rosrun random_walk random_walk
*   rosrun grid_mapper grid_mapper 100 100





![swearingen](images/swearingen.png)
![lowres](images/lowres.png)
![highres](images/highres.png)